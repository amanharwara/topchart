## High Priority

- Rip out everything and start again with mobile-first markup
- Implement download
- Implement backup/restore

## Medium Priority

- Zoom; using scroll, using options

## Low Priority / Potential Future Features

- List 10 recently used colors for background/foreground color
- Randomize search suggestions
- Potentially rewrite chart store using Jotai
